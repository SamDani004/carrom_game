from carrom_enums import CoinStatus


class CarromCoin:
    def __init__(self, coin_type):
        """
            Initialises the carrom coins
            :param coin_type: Type of the coin
        """
        self.coin_type = coin_type
        self.coin_status = CoinStatus.OPEN
        self.pocketer = None

    def set_status(self, coin_status, pocketer):
        """
        Sets the status of the coin
        :param coin_status: Coin status
        :param pocketer: Name of the player who pocketed the coin
        :return: None
        """
        self.coin_status = coin_status
        self.pocketer = pocketer
