from carrom_game import CarromGame
from carrom_enums import GameStatus
from carrom_exception import CARROM_EXCEPTION_MESSAGES, CarromException

def execute_game(turns, players):
    """
    executes the game and return the result
    :param turns:
    :param players:
    :return: The game str after executing
    """
    cg = CarromGame(players)
    result = []
    i = 0
    while cg.game_state == GameStatus.PROGRESS and i < len(turns):
        turn_result = cg.execute_turn(turns[i])
        result.append(turn_result)
        i += 1
    return result


def construct_res(player_name, game_state, won_player, players, player_scores, exception_msg = None):
    """
    construct the turn msg
    :param player_name: name of the player
    :param game_state: state of game
    :param players: list of players
    :param player_scores: scores of player
    :return: turn msg
    """
    scores = ",".join(["{}={}".format(players[i], str(player_scores[i])) for i in range(len(players))])
    res = "{}:{}:{}:{}".format(player_name, game_state, won_player, scores)
    if exception_msg:
        res = "{}:{}".format(res, exception_msg)
    return res


def test_a_win_normal():
    """Player A win"""
    players = ["sam", "dani"]
    turns = ["R", "B", "B,B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 1]),
        construct_res("sam", GameStatus.WON, "sam", players, [5,1])
    ]
    assert execute_game(turns, players) == res


def test_b_win_normal():
    """Player B win"""
    players = ["sam", "dani"]
    turns = ["B", "B", "", "B,B,B", "", "B,B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 1]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [1,1]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("dani", GameStatus.WON, "dani", players, [1, 5]),
    ]
    assert execute_game(turns, players) == res


def test_b_win_opponent_foul():
    """Player B wins by opponent losing points"""
    players = ["sam", "dani"]
    turns = ["R", "B", "", "B,B,B", "", "B,B" , "ST", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [3, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 1]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3,1]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3, 3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 5]),
        construct_res("sam", GameStatus.WON, "dani", players, [2, 5])
    ]
    assert execute_game(turns, players) == res


def test_a_win_opponent_foul():
    """Player A wins by opponent losing points"""
    players = ["sam", "dani"]
    turns = ["B", "R", "B,B", "ST,BD", "B", "ST", "B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3,3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 1]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [4, 1]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [4, 0]),
        construct_res("sam", GameStatus.WON, "sam", players, [5, 0])
    ]
    assert execute_game(turns, players) == res


def test_a_invalid_input():
    """Invalid input test by player A"""
    players = ["sam", "dani"]
    turns = ["B", "R", "B,B", "ST,BD", "R", "ST", "B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3,3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 1]),
        construct_res("sam", GameStatus.EXCEPTION, "-", players, [3, 1],"CUSTOM EXCEPTION OCCURRED:{}".format(
            CarromException('INVALID_STRIKE_COIN', CARROM_EXCEPTION_MESSAGES['INVALID_STRIKE_COIN'])))
    ]
    assert execute_game(turns, players) == res


def test_b_invalid_input():
    """Invalid input test by player B"""
    players = ["sam", "dani"]
    turns = ["B,B", "B,B", "B", "B,B", "R", "RD", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [2, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [2, 2]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3,2]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 4]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [6, 4]),
        construct_res("dani", GameStatus.EXCEPTION, "-", players, [6, 4],"CUSTOM EXCEPTION OCCURRED:{}".format(
            CarromException('INVALID_STRIKE_COIN', CARROM_EXCEPTION_MESSAGES['INVALID_STRIKE_COIN'])))
    ]
    assert execute_game(turns, players) == res


def test_game_draw_exhaust_coins():
    """Game draw test when all coins are exhausted"""
    players = ["sam", "dani"]
    turns = ["B", "R", "B,B", "B,B", "B,B", "B,B", "B,B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-" ,players, [1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3,3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 5]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [5, 5]),
        construct_res("dani", GameStatus.DRAW, "-", players, [5, 7]),
    ]
    assert execute_game(turns, players) == res


def test_game_draw_exhaust_turns():
    """Game draw test when all turns are exhausted"""
    players = ["sam", "dani"]
    turns = ["B", "R", "B,B", "B,B", "B,B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-", players, [1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3, 3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [3, 5]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [5, 5]),
        construct_res("dani", GameStatus.DRAW, "-", players, [5, 5])
    ]
    assert execute_game(turns, players) == res


def test_a_red_strike_rule():
    """Red strike rule test for player A"""
    players = ["sam", "dani"]
    turns = ["R,B,B,BD", "B", "B,B", "B", "B,B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-", players, [2, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [2, 1]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [4, 1]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [4, 2]),
        construct_res("sam", GameStatus.WON, "sam", players, [6, 2]),
    ]
    assert execute_game(turns, players) == res


def test_b_red_strike_rule():
    """Red strike rule test for player B"""
    players = ["sam", "dani"]
    turns = ["BD", "R,B,B", "B,B", "B", "B,B", "B,B", "B", "-"]
    res = [
        construct_res("sam", GameStatus.PROGRESS, "-", players, [-1, 0]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [-1, 3]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [1, 3]),
        construct_res("dani", GameStatus.PROGRESS, "-", players, [1, 4]),
        construct_res("sam", GameStatus.PROGRESS, "-", players, [3, 4]),
        construct_res("dani", GameStatus.WON, "dani", players, [3, 6]),
    ]
    assert execute_game(turns, players) == res
