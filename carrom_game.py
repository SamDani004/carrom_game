from carrom_player import CarromPlayer
from carrom_enums import CoinType
from carrom_coin import CarromCoin
from carrom_enums import CoinStatus
from carrom_exception import CarromException
from carrom_enums import GameStatus
from carrom_exception import CARROM_EXCEPTION_MESSAGES
from carrom_enums import TurnOutcomeType

# Indicates the end of all turns
END_TURN = "-"


class CarromGame:
    def __init__(self, players):
        """
        Initialises the carrom game
        :param players: list of names of players playing the game
        """
        self.players = [CarromPlayer(player_name) for player_name in players]
        self.player_count = len(self.players)
        self.black_coins_count = 9
        self.red_coins_count = 1
        self.coins_available = {
            CoinType.BLACK.value: [CarromCoin(CoinType.BLACK.value) for _ in range(self.black_coins_count)],
            CoinType.RED.value: [CarromCoin(CoinType.RED.value) for _ in range(self.red_coins_count)]
        }
        self.active_player_idx = 0
        self.game_state = GameStatus.PROGRESS
        self.won_player = None

    def __execute_end_game_rules(self):
        """
        Executes the end game rules defined and checks if endgame reached
        :return: The game state after current turn
        """
        first_max_player = max(self.players[0], self.players[1], key=lambda x: x.total_score)
        second_max_player = min(self.players[0], self.players[1], key=lambda x: x.total_score)
        for i in range(2, self.player_count):
            if self.players[i].total_score > first_max_player.total_score:
                second_max_player = first_max_player
                first_max_player = self.players[i]
            elif self.players[i].total_score > second_max_player.total_score:
                second_max_player = self.players[i]
        if first_max_player.total_score >= 5 and first_max_player.total_score - second_max_player.total_score >= 3:
            self.won_player = first_max_player.player_name
            return GameStatus.WON
        if not len(self.coins_available[CoinType.BLACK.value]) + len(self.coins_available[CoinType.RED.value]):
            return GameStatus.DRAW
        return GameStatus.PROGRESS

    def __execute_pocket_rules(self, strike_coins_count):
        """
        Executes the pocket rules defined and returns the score for the turn
        :param coins_pocketed: dict of coins pocketed in the turn along with their types
        :return: the score for the turn
        """
        if strike_coins_count.get(CoinType.RED.value, 0) == 1:
            strike_coins_count[CoinType.BLACK.value] = 0
        if strike_coins_count.get(CoinType.BLACK.value, 0) > 2:
            strike_coins_count[CoinType.BLACK.value] = 2
        return strike_coins_count

    def __execute_score_rules(self, cur_score):
        """
        Executes the score rules defined and return if any additional scores for the player
        :param person_score_history: list containing the history of the scores in each turn for a person
        :return: additional score for the turn
        """
        player_score_history = self.players[self.active_player_idx].score_history
        turns_count = len(player_score_history)
        if turns_count < 2:
            return 0
        penalty_score = 0
        empty_turns_count = 0
        foul_turns_count = 0
        for turn in range(0, turns_count):
            turn_result = player_score_history[turn][0]
            if turn >= turns_count - 2 and turn_result == TurnOutcomeType.NONE:
                empty_turns_count += 1
            elif turn_result == TurnOutcomeType.FOUL:
                foul_turns_count += 1
        if not isinstance(cur_score, int):
            empty_turns_count += 1
        elif cur_score < 0:
            foul_turns_count += 1

        if empty_turns_count == 3:
            penalty_score -= 1
        if cur_score and cur_score < 0 and not foul_turns_count % 3:
            penalty_score -= 1
        return penalty_score

    def __check_coins_validity(self, strike_map):
        """
        Checks id the coins pocketed in the current turn are actually available
        :param strike_map:
        :return:
        """
        red_coins_count = strike_map.get("RD", 0) + strike_map.get("R", 0)
        black_coins_count = strike_map.get("BD", 0) + strike_map.get("B", 0)
        if (red_coins_count > len(self.coins_available[CoinType.RED.value])) or \
                (black_coins_count > len(self.coins_available[CoinType.BLACK.value])):
                    raise CarromException('INVALID_STRIKE_COIN', CARROM_EXCEPTION_MESSAGES['INVALID_STRIKE_COIN'])

    def __get_turn_result(self, exception_msg = None):
        """
        Generates the turn outcome string of format
                "CurPlayer Name:Game Status:Current Scores of the players"
        :param exception_msg:
        :return: The turn outcome string
        """
        player_won = "-"
        if self.game_state == GameStatus.WON:
            player_won = self.won_player
        res = "{}:{}:{}:{}".format(self.players[self.active_player_idx].player_name, self.game_state, player_won,
                                 ",".join(["{}={}".format(player.player_name, player.total_score)
                                           for player in self.players]))
        if exception_msg:
            res = "{}:{}".format(res, exception_msg)
        return res

    def execute_turn(self, strike_coins):
        """
        Executes the actual turn by updating the coin and player status and also executes
        the different kinds of rules
        :param strike_coins: The coins involved in the particular strike
        :return: Turn outcome string of format "CurPlayer Name:Game Status:Current Scores of the players"
        """
        try:
            cur_player = self.players[self.active_player_idx]
            if strike_coins == END_TURN:
                self.game_state = GameStatus.DRAW
            else:
                strike_coins_count = {}
                strike_coins_split = strike_coins.split(",")
                for strike_coin in strike_coins_split:
                    strike_coins_count.update({strike_coin: strike_coins_count.get(strike_coin, 0) + 1})
                self.__check_coins_validity(strike_coins_count)
                strike_coins_count.update(self.__execute_pocket_rules(strike_coins_count))
                score = None
                for coin_type in strike_coins_count:
                    for i in range(strike_coins_count[coin_type]):
                        if not coin_type:
                            break
                        if not score:
                            score = 0
                        if coin_type == CoinType.BLACK.value:
                            score += 1
                            self.coins_available[CoinType.BLACK.value].pop().set_status(CoinStatus.POCKETED, cur_player.player_name)
                        elif coin_type == CoinType.RED.value:
                            score += 3
                            self.coins_available[CoinType.RED.value].pop().set_status(CoinStatus.POCKETED, cur_player.player_name)
                        elif coin_type == CoinType.STRIKER.value:
                            score -= 1
                        elif coin_type == CoinType.BLACK_DEFUNCT.value:
                            score -= 1
                            self.coins_available[CoinType.BLACK.value].pop().set_status(CoinStatus.OBSOLETE, cur_player.player_name)
                        elif coin_type == CoinType.RED_DEFUNCT.value:
                            score -= 1
                            self.coins_available[CoinType.RED.value].pop().set_status(CoinStatus.OBSOLETE, cur_player.player_name)
                turn_outcome = TurnOutcomeType.NONE
                if isinstance(score, int) and score >= 0:
                    turn_outcome = TurnOutcomeType.GAIN
                elif isinstance(score, int) and score < 0:
                    turn_outcome = TurnOutcomeType.FOUL
                if isinstance(score, int):
                    score += self.__execute_score_rules(score)
                else:
                    score = self.__execute_score_rules(score)
                cur_player.add_score(score, turn_outcome)
                self.game_state = self.__execute_end_game_rules()
            turn_result = self.__get_turn_result()
            self.active_player_idx = (self.active_player_idx + 1) % self.player_count
            return turn_result
        except CarromException as known_exception:
            self.game_state = GameStatus.EXCEPTION
            return self.__get_turn_result("CUSTOM EXCEPTION OCCURRED:{}".format(known_exception))
        except Exception as unknown_exception:
            self.game_state = GameStatus.EXCEPTION
            return self.__get_turn_result("UNKNOWN EXCEPTION OCCURRED:".format(unknown_exception))


if __name__ == "__main__":
    cg = CarromGame(["sam", "dani","jawa"])
    turns = ["R", "BD", "", "BD", "", "BD", "", "", "B,B,B", "zST", "-"]
    i = 0
    while cg.game_state == GameStatus.PROGRESS and i < len(turns):
        turn_result = cg.execute_turn(turns[i])
        print(turn_result)
        i += 1
