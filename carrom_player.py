class CarromPlayer:
    def __init__(self, player_name):
        """
        Initialises the player
        :param player_name: Name of the player
        """
        self.player_name = player_name
        self.score_history = []
        self.total_score = 0

    def add_score(self, score, turn_outcome):
        """
        Adds the score to the player and also adds the turn outcome to player history
        :param score: Score to be added to the player
        :param turn_outcome: Turn outcome for the turn
        :return: None
        """
        self.score_history.append([turn_outcome, score])
        self.total_score += score
