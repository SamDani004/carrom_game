CARROM_EXCEPTION_MESSAGES = {
    "INVALID_STRIKE_COIN": "Invalid strike coin input. Coin of specified type is unavailable"
}

class CarromException(Exception):
    def __init__(self, error_code, error_msg):
        """
        Initializes the Carrom Custom Exception
        :param error_code: Error code
        :param error_msg: Error msg
        """
        self.error_code = error_code
        self.error_msg = error_msg

    def __str__(self):
        """
        Overrides the default str method of object
        :return: String representation of Exception object
        """
        return self.error_code + ":" + self.error_msg
