import enum


class CoinType(enum.Enum):
    """
        Types of Coin
    """
    RED = 'R'
    BLACK = 'B'
    STRIKER = 'ST'
    RED_DEFUNCT = 'RD'
    BLACK_DEFUNCT = 'BD'


class CoinStatus(enum.Enum):
    """
        States that a coin can be
    """
    OPEN = 1
    POCKETED = 2
    OBSOLETE = 3


class TurnOutcomeType(enum.Enum):
    """
        Outcomes on a turn for a player
    """
    GAIN = 1
    FOUL = 2
    NONE = 3


class GameStatus(enum.Enum):
    """
        States of the carrom game
    """
    PROGRESS = (1, "GAME PROGRESS")
    WON = (2, "GAME WON")
    DRAW = (3, "GAME DRAW")
    EXCEPTION = (4, "EXCEPTION OCCURRED")

    def __init__(self, game_status_id, game_status_name):
        self.status_id = game_status_id
        self.status_name = game_status_name

    def get_name(self):
        return self.status_name


class StrikeType(enum.Enum):
    """
        Types of strike based on coins pocketed in that turn
    """
    SINGLE_STRIKE = 1
    MULTI_STRIKE = 2
    RED_STRIKE = 3
    STRIKER_STRIKE = 4
    DEFUNCT_STRIKE = 5
    EMPTY_STRIKE = 6
